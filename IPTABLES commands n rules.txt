Firewall:
service iptables start
service iptables stop
service iptables status


service iptables save
iptables-save


iptables-restore < Directory/filename

Iptables location:

/etc/sysconf/iptables

/etc/sysconf/iptables.save

To delete rule:
iptables -D Input/OUTPUT rulenumber

To Insert:
iptables -I INPUT rulenumber_tobe_placed rule_defined

for e.g
-I INPUT 2 -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT

-A Appends the rules that is place the rule in the end of the iptables list

-A INPUT -s 10.16.130.0/24 -p tcp -m multiport --dports 9000,9001,9002,9003,9004 -m state --state NEW,ESTABLISHED -j ACCEPT

-A INPUT -s 10.16.139.142/32 -p tcp -m tcp --dport 5901 -m state --state NEW,ESTABLISHED -j ACCEPT

-A OUTPUT -d 10.200.69.29/32 -p tcp -m multiport --dports 7777,4443 -m state --state NEW,ESTABLISHED -j ACCEPT

-A OUTPUT -d 10.200.69.4/32 -p tcp -m tcp --dport 1522 -m state --state NEW,ESTABLISHED -j ACCEPT



VPN IP :iptables -I INPUT 17 -s 10.16.4.0/24 -p tcp -m multiport --dports 22,1522 -m state --state NEW,ESTABLISHED -j ACCEPT


iptables -I INPUT -s 10.16.132.0/24 -p tcp -m tcp --dport 443 -m state --state NEW,ESTABLISHED -j ACCEPT

iptables -I INPUT 2 -p tcp -m state --state NEW -m tcp --dport 443 -j ACCEPT


-A INPUT -p icmp -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited

-start up on
chkconfig --level 2345 iptables on